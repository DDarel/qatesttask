﻿Feature: Spotify

Scenario: Test Login form with empty credentials
	Given User opened Spotify site
	When User enter LogIn info
	| UserName | Password |
	| Admin | admin |
	And User clear inputs
	And Click LogIn button
	Then User check the error messages

Scenario: Test Login form with incorrect credentials
	Given User opened Spotify site
	When User enter LogIn info
	| UserName | Password |
	| Admin | admin |
	And Click LogIn button
	Then User check the error messages

Scenario: Test Login form with correct credentials
	Given User opened Spotify site
	When User enter LogIn info
	| UserName | Password |
	| darel.dahanavar.shevchyk@gmail.com | 004039Vlad |
	And Click LogIn button
	Then User check succes LogIn name DDarel