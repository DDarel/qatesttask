﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyTest.Pages
{
    internal class UserPage
    {
        private IWebDriver _webDriver;
        private WebDriverWait _wait;
        private IWebElement _userName;

        public IWebDriver webDriver { get { return _webDriver; } }
        public WebDriverWait wait { get { return _wait; } }
        public IWebElement userName { get { return _userName; } }

        private UserPage() { }

        public class UserPageBuilder
        {

            private readonly UserPage userPage;
            public UserPageBuilder(IWebDriver webDriver)
            {
                userPage = new UserPage();
                userPage._webDriver = webDriver;
                userPage._wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            }
            public UserPageBuilder WithUserName()
            {
                userPage._userName = WaitToFindElement(userPage._webDriver, userPage._wait, By.XPath("//h1[@dir=\"auto\"]"));
                return this;
            }
            public UserPage build() { return userPage; }
        }

        static IWebElement WaitToFindElement(IWebDriver driver, WebDriverWait wait, By element)
        {
            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(element));
                IWebElement _element = driver.FindElement(element);
                return _element;
            }
            catch
            {
                throw new Exception($"Element was not found");
            }
        }

    }
}
