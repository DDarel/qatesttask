﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;

namespace SpotifyTest.Pages
{
    internal class HomePage
    {
        private IWebDriver _webDriver;
        private WebDriverWait _wait;
        private IWebElement _logInBtn;
        private IWebElement _userName;

        public IWebDriver webDriver { get { return _webDriver; } }
        public WebDriverWait wait { get { return _wait; } }
        public IWebElement logInBtn { get { return _logInBtn; } }
        public IWebElement userName { get { return _userName; } }

        private HomePage() { }

        public class HomePageBuilder
        {

            private readonly HomePage homePage;
            public HomePageBuilder(IWebDriver webDriver)
            {
                homePage = new HomePage();
                homePage._webDriver = webDriver;
                homePage._wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            }
            public HomePageBuilder WithSignUpButton()
            {
                homePage._logInBtn = WaitToFindElement(homePage._webDriver, homePage._wait, By.XPath("//*[@data-testid=\"login-button\"]"));
                return this;
            }

            public HomePageBuilder WithUserName()
            {
                homePage._userName = WaitToFindElement(homePage._webDriver, homePage._wait, By.XPath("//*[@data-testid=\"login-button\"]"));
                return this;
            }
            public HomePage build() { return homePage; }
        }

        static IWebElement WaitToFindElement(IWebDriver driver, WebDriverWait wait, By element)
        {
            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(element));
                IWebElement _element = driver.FindElement(element);
                return _element;
            }
            catch
            {
                throw new Exception($"Element was not found");
            }
        }

        public void GoToProfile() {
            IWebElement userIco = WaitToFindElement(webDriver, wait, By.XPath("//*[@data-testid=\"user-widget-link\"]"));
            userIco.Click();
            IWebElement profileBtn = WaitToFindElement(webDriver, wait, By.XPath("//*[@data-testid=\"user-widget-menu\"]//li[2]/a"));
            profileBtn.Click();
        }
    }
}
