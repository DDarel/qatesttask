﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;

namespace SpotifyTest.Pages
{
    internal class LogInPage
    {
        private IWebDriver _webDriver;
        private WebDriverWait _wait;
        private IWebElement _logInSubmitBtn;
        private IWebElement _userNameTxt;
        private IWebElement _passwordTxt;
        private IWebElement _alertDiv;

        public IWebDriver webDriver { get { return _webDriver; } }
        public WebDriverWait wait { get { return _wait; } }
        public IWebElement logInSubmitBtn { get { return _logInSubmitBtn; } }
        public IWebElement userNameTxt { get { return _userNameTxt; } }
        public IWebElement passwordTxt { get { return _passwordTxt; } }
        public IWebElement alertDiv { get { return _alertDiv; } }

        private LogInPage() { }

        public class LogInPageBuilder
        {
            private readonly LogInPage logInPage;
            public LogInPageBuilder(IWebDriver webDriver)
            {
                logInPage = new LogInPage();
                logInPage._webDriver = webDriver;
                logInPage._wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            }
            public LogInPageBuilder WithLogInSubmitButton()
            {
                logInPage._logInSubmitBtn = WaitToFindElement(logInPage._webDriver, logInPage._wait, By.XPath("//*[@id=\"login-button\"]"));
                return this;
            }
            public LogInPageBuilder WithUserNameTextArea()
            {
                logInPage._userNameTxt = WaitToFindElement(logInPage._webDriver, logInPage._wait, By.XPath("//*[@id=\"login-username\"]"));
                return this;
            }
            public LogInPageBuilder WithPasswordTextArea()
            {
                logInPage._passwordTxt = WaitToFindElement(logInPage._webDriver, logInPage._wait, By.XPath("//*[@id=\"login-password\"]"));
                return this;
            }
            public LogInPageBuilder WithAlert()
            {
                logInPage._alertDiv = WaitToFindElement(logInPage._webDriver, logInPage._wait, By.XPath("//*[@class=\"Wrapper-sc-62m9tu-0 dkCVSE encore-negative-set sc-cwSeag KisiB\"]"));
                return this;
            }
            public LogInPage build() { return logInPage; }
        }

        static IWebElement WaitToFindElement(IWebDriver driver, WebDriverWait wait, By element)
        {
            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(element));
                IWebElement _element = driver.FindElement(element);
                return _element;
            }
            catch
            {
                throw new Exception($"Element was not found");
            }
        }

        public void EnterUserNameAndPassword(string userName, string password)
        {
            userNameTxt.SendKeys(userName);
            passwordTxt.SendKeys(password);
        }

        public void ClearInputs() {
            userNameTxt.Clear();
            passwordTxt.Clear();
        }
    }
}
