using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpotifyTest.Pages;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TechTalk.SpecFlow.Infrastructure;
using static SpotifyTest.Pages.HomePage;
using static SpotifyTest.Pages.LogInPage;
using static SpotifyTest.Pages.UserPage;

namespace SpotifyTest.StepDefinitions
{
    [Binding]
    public class SpotifyStepDefinitions
    {

        IWebDriver webDriver = new ChromeDriver();

        [Given(@"User opened Spotify site")]
        public void GivenUserOpenedSpotifySite()
        {
            webDriver.Navigate().GoToUrl("https://open.spotify.com/");
            HomePage homePage = new HomePageBuilder(webDriver).WithSignUpButton().build();
            homePage.logInBtn.Click();
        }

        [When(@"User enter LogIn info")]
        public void WhenUserEnterLogInInfo(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            LogInPage logInPage = new LogInPageBuilder(webDriver).WithPasswordTextArea().WithUserNameTextArea().build();
            logInPage.EnterUserNameAndPassword((string)data.UserName, (string)data.Password);
        }

        [When(@"User clear inputs")]
        public void WhenUserClearInputs()
        {
            LogInPage logInPage = new LogInPageBuilder(webDriver).WithPasswordTextArea().WithUserNameTextArea().build();
            logInPage.ClearInputs();
        }

        [When(@"Click LogIn button")]
        public void WhenClickLogInButton()
        {
            LogInPage logInPage = new LogInPageBuilder(webDriver).WithLogInSubmitButton().build();
            logInPage.logInSubmitBtn.Click();
        }

        [Then(@"User check the error messages")]
        public void ThenUserCheckTheErrorMessages()
        {
            LogInPage logInPage = new LogInPageBuilder(webDriver).WithAlert().build();
            if (logInPage.alertDiv == null) {
                throw new Exception($"There was no error message");
            }
        }

        [Then(@"User check succes LogIn name (.*)")]
        public void ThenUserCheckSuccesLogIn(string userName)
        {
            HomePage homePage = new HomePageBuilder(webDriver).build();
            homePage.GoToProfile();
            UserPage userPage = new UserPageBuilder(webDriver).WithUserName().build();
            if (!userPage.userName.Text.Equals(userName)) {
                throw new Exception($"UserName is not correct");
            }
        }
    }
}
